# Project Proposal

## Create a Python Script for use in an ArcGIS Pro toolbox
### Erasing multiple polygons from each other in descending date order and then merging those polygons into a single shapefile.

Producing daily growth wildfire perimeters is a cumbersome task.  The National Interagency Fire Center produces daily fire perimeter polygon shapefiles for every major wildfire in the US.  These files, among many others, are uploaded to their FTP site.  Each daily fire perimeter polygon encompasses the entire fire perimeter from the ignition date to the date in which the perimeter is mapped (using satellite data).  To create a daily spread wildfire map, the previous day’s perimeter polygon would need to be erased from the current days’ polygon.  If you are mapping an active fire, this process is short and only done daily.  If you want to do similar mapping for older fires in order to perform analysis that involves fire spread area and rates, this could mean erasing the previous day’s perimeter 30 times for a fire that lasted 30 days.  The individual daily fire polygons could be used individually or combined to create a single shapefile.
Example: Cameron Peak Fire – Ignition 8/15/20 – currently burning as of 10/23/20
The Cameron Peak Fire in Colorado currently has 47 separate perimeter polygon shapefiles which would have to be erased in reverse chronological order to get the daily burn polygons.

The individual fire perimeter shapefiles downloaded from the NIFC FTP site and put into a folder will naturally line up in date order given the naming schema used by NIFC.
20201003_2205_CameronPeak_HeatPerimeter.shp
20201008_2055_CameronPeak_HeatPerimeter.shp
20201013_2003_CameronPeak_HeatPerimeter.shp

The goal of the tool would be to be able to put all fire perimeter polygon shapefiles into a folder, erase each polygon with the previous date polygon, append a standard moniker such as “_erase” to the end of each output shapefile, then merge the shapefiles with  “_erase” into a merged polygon fire perimeter file.

Tools to be used:

Erase (Analysis) - Creates a feature class by overlaying the input features with the polygons of the erase features. Only those portions of the input features falling outside the erase features outside boundaries are copied to the output feature class.
https://pro.arcgis.com/en/pro-app/tool-reference/analysis/erase.htm
Erase(in_features, erase_features, out_feature_class, {cluster_tolerance})

Merge (Data Management) - Combines multiple input datasets into a single, new output dataset. This tool can combine point, line, or polygon feature classes or tables.
https://pro.arcgis.com/en/pro-app/tool-reference/data-management/merge.htm
Merge(inputs, output, {field_mappings}, {add_source})

Add Field (Data Management)
Adds a new field to a table or the table of a feature class, feature layer, and/or rasters with attribute tables.
AddField(in_table, field_name, field_type, {field_precision}, {field_scale}, {field_length}, {field_alias}, {field_is_nullable}, {field_is_required}, {field_domain})

The attribute tables of the fire perimeter polygons all have the following fields: FID, Shape, id, GIS_Acres.
There is no field that distinguishes one fire perimeter from another, so when a merge is ran, the polygon is not useful as it cannot be symbolized or analyzed by date.  A date field would need to be added to the shapefiles before the merge.  This would best be done to all shapefiles before they are erased.  The date field could be populated from the first 8 digits of the shapefile name.
The basic tools within arcpy are straightforward. The complexity comes from having to select the correct previous date shapefile as the erase feature, then keep selecting backwards for each erase.  

To complete this project, I will have to learn to iterate over a list of shapefiles in the correct order, append common filename endings to each output file, pull portions of a filename to use as the field name.  

