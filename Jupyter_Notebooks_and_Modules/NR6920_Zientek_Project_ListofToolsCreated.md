# Fire Perimeter Mapping Script

## Create a Python Script for use in an ArcGIS Pro Toolbox
### Erasing multiple polygons from each other in descending date order and then merging those polygons into a single shapefile.


## Problem
Producing daily growth wildfire perimeters is a cumbersome task.  The National Interagency Fire Center produces daily fire perimeter polygon shapefiles for every major wildfire in the US.  These files, among many others, are uploaded to their FTP site.  Each daily fire perimeter polygon encompasses the entire fire perimeter from the ignition date to the date in which the perimeter is mapped (using satellite data).  To create a daily spread wildfire map, the previous day’s perimeter polygon would need to be erased from the current days’ polygon.  If you are mapping an active fire, this process is short and only done daily.  If you want to do similar mapping for older fires in order to perform analysis that involves fire spread area and rates, this could mean erasing the previous day’s perimeter 30 times for a fire that lasted 30 days.  The individual daily fire polygons could be used individually or combined to create a single shapefile.
Example: Cameron Peak Fire – Ignition 8/15/20 – currently burning as of 10/23/20
The Cameron Peak Fire in Colorado currently has 47 separate perimeter polygon shapefiles which would have to be erased in reverse chronological order to get the daily burn polygons.

## Goal
The goal of the tool would be to be able to put all fire perimeter polygon shapefiles into a folder, erase each polygon with the previous date polygon, append a standard moniker such as “_eraseYYYYMMDD” to the end of each output shapefile, then merge the shapefiles with  “_eraseYYYYMMDD” into a merged polygon fire perimeter file.

## Deliverables

#### 1. Jupyter Notebook script to be used separately or within ArcGIS Pro.
##### NR6920-zientek-project-juputer-notebook.ipynb 
This notebook has detailed instructions on how the tool works and the type and format of the data required to successfully run. It was written for someone with limited python knowledge as only the folder path and out file name need to be entered in the variables listed in the first code block.

#### 2. Jupyter Notebook with individual functions created for the Add Field Fill In Date script and for the Erase and Merge script.
##### NR6920-zientek-project-function.ipynb 
Two functions were created, one that adds a field ('Date') to each shapefile then fills in the rows under the new field with the date (format YYYYMMDD) that is at beginning of each file name.  The second function erases the previous date shapefile from the one immediately following and creates a new shapefile with "_eraseYYYYMMDD" appended to the end of the file with the YYYYMMDD signifying the date of the shapefile used as the erase feature.  The initial date (earliest date) shapefile and the erase shapefiles are merged into a single shapefile.

#### 3. Jupyter Notebook and Module with tools created for the Add Field Fill In Date script and for the Erase and Merge script.
##### NR6920-zientek-project-module.ipynb 
##### fire_tool.py
A module (fire_tools) was created that holds two tools: fp_add_date_field and fp_erase_merge.

#### 4. ArcGIS Pro Toolbox and script.
##### NR6920_Zientek_Project_arcGISProTool.tbx 
##### zientek_project_arcgisprotool.py




