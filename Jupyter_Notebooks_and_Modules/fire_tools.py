

def fp_add_date_field(folder):
    import arcpy
    arcpy.env.workspace = folder
    arcpy.env.overwriteOutput = True
    field_name = 'Date'
    fc = arcpy.ListFeatureClasses()
    for file in fc:
        arcpy.AddField_management(file, field_name, 'LONG')
        with arcpy.da.UpdateCursor(file, field_name) as updater:
            for row in updater:
                updater.updateRow([file[0:8]])



def fp_erase_merge(folder, merge_outfile):
    import arcpy
    arcpy.env.workspace = folder
    arcpy.env.overwriteOutput = True
    fc = arcpy.ListFeatureClasses()
    merge_list = []
    merge_list.append(fc[0])
    for x in range(len(fc)-1):
        in_features = fc[x+1]
        erase_features = fc[x]
        out_fc = in_features.replace(".shp", f"_erase{erase_features[0:8]}.shp")
        arcpy.Erase_analysis(in_features, erase_features, out_fc)
        merge_list.append(out_fc)
    arcpy.Merge_management(merge_list, merge_outfile)







