## Fire Perimeters - Add Field, Erase, Merge Tool
##### NR6920_Zientek_Project_arcGISProTool.tbx 
##### zientek_project_arcgisprotool.py

### Installation
Copy the toolbox and script to your computer;
Open an ArcGIS Pro project;
Right click on Toolboxes in the Catalog pane;
Select 'Add Toolbox';
Go the location where you saved the toolbox file and select it;
Click the arrow next to the toolbox to show the script tool;
Right click on the Fire Perimeters - Add Field, Erase, Merge tool and select 'Properties';
Under 'General', click the folder under 'Script File' and browse to where you stored the .py file and select it;
Click OK.

### To Run the Tool
Double Click the Fire Perimeters - Add Field, Erase, Merge tool;
Either select the fire perimeters from the ToC or click the file icon and browse to the folder where the are stored and select;
**NOTE** Fire Perimeter polygon (must be polygons) shapefiles and in ascending order (earliest date first and top of list of files)
Enter the filename and location for the merged fire perimeter file (can only be saved as .shp file);
Click Run.