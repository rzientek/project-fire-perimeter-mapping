import arcpy
import os

fc = arcpy.GetParameterAsText(0).split(';')
merge_outfile = arcpy.GetParameterAsText(1)

field_name = 'Date'
for file in fc:
    arcpy.AddField_management(file, field_name, 'LONG')
    head, tail = os.path.split(file)
    with arcpy.da.UpdateCursor(file, field_name) as updater:
        for row in updater:
            updater.updateRow([tail[0:8]])
merge_list = []
merge_list.append(fc[0])
for x in range(len(fc)-1):
    in_features = fc[x+1]
    erase_features = fc[x]
    head, tail = os.path.split(erase_features)
    out_fc = in_features.replace(".shp", f"_erase{tail[0:8]}.shp")
    arcpy.Erase_analysis(in_features, erase_features, out_fc)
    merge_list.append(out_fc)
arcpy.Merge_management(merge_list, merge_outfile)

