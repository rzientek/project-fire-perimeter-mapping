# ArcGIS Pro tool to batch erase multiple fire perimeters.  


## Create a Python Script for use in an ArcGIS Pro Toolbox
### 1. Adds a new field to each fire perimeter polygon shapefile and fills in the date of the fire perimeter into each row; then
### 2. Erases multiple polygons from each other in descending date order (previous date polygon used as erase feature) and then merges those polygons into a single shapefile.


## Problem
Producing daily growth wildfire perimeters is a cumbersome task.  The National Interagency Fire Center produces daily fire perimeter polygon shapefiles for every major wildfire in the US.  These files, among many others, are uploaded to their FTP site.  Each daily fire perimeter polygon encompasses the entire fire perimeter from the ignition date to the date in which the perimeter is mapped (using satellite data).  To create a daily spread wildfire map, the previous day’s perimeter polygon would need to be erased from the current days’ polygon.  If you are mapping an active fire, this process is short and only done daily.  If you want to do similar mapping for older fires in order to perform analysis that involves fire spread area and rates, this could mean erasing the previous day’s perimeter 30 times for a fire that lasted 30 days.  The individual daily fire polygons could be used individually or combined to create a single shapefile. The fire perimeter polygons do not have a date field in the attribute table, so this field would need to be added to every fire perimeter shapefile and corresponding dates populated before erasing and merging or there would be no way to symbolize the fire spread by date.

Example: Cameron Peak Fire – Ignition 8/15/20
The Cameron Peak Fire in Colorado has over 50 separate perimeter polygon shapefiles.  In order to create a fire progression shapefile, each polygon shapefile would have to be erased in reverse chronological order to get the daily burn polygons, then merged into a single shapefile.

The individual fire perimeter shapefiles downloaded from the NIFC FTP site and put into a folder will naturally line up in date order given the naming schema used by NIFC.
20201003_2205_CameronPeak_HeatPerimeter.shp
20201008_2055_CameronPeak_HeatPerimeter.shp
20201013_2003_CameronPeak_HeatPerimeter.shp

## Goal
The goal of the tool would be to be able to put all fire perimeter polygon shapefiles into a folder, then select, or select from ArcGIS Pro map table of contents; add a new field  called 'Date' to each fire perimeter shapefile and populate the rows with the date (YYYYMMDD) from the date at the beginning of each file name; erase each polygon with the previous date polygon as the erase feature, append a standard moniker such as '_eraseYYYYMMDD' to the end of each output shapefile using the date from the name of the erase feature file name, then merge the initial fire perimeter shapefile with the erased shapefiles with '_eraseYYYYMMDD' into a merged polygon fire perimeter file.

## ArcGIS Pro Toolbox and  Python script
#### The toolbox and python script are located in the ArcGISPro_Toolbox folder
##### NR6920_Zientek_Project_arcGISProTool.tbx 
##### zientek_project_arcgisprotool.py
##### FPEAM_readme.md

### Installation
###### Copy the toolbox and script to your computer;
###### Open an ArcGIS Pro project;
###### Right click on Toolboxes in the Catalog pane;
###### Select 'Add Toolbox';
###### Go the location where you saved the toolbox file and select it;
###### Click the arrow next to the toolbox to show the script tool;
###### Right click on the Fire Perimeters - Add Field, Erase, Merge tool and select 'Properties';
###### Under 'General', click the folder under 'Script File' and browse to where you stored the .py file and select it;
###### Click OK.

### To Run the Tool
###### Double Click the Fire Perimeters - Add Field, Erase, Merge tool;
###### Either select the fire perimeters from the ToC or click the file icon and browse to the folder where the are stored and select;
###### **NOTE** Fire Perimeter polygon shapefiles must be in ascending order (earliest date first and top of list of files)
###### Enter the filename and location for the merged fire perimeter file (can only be saved as .shp file);
###### Click Run.

### There is clean test data to use in the Test_Data_Clean folder

## Additional Scripts, Modules, and Tools created during this project:
#### These can be found in the Jupyter_Notebooks_and_Modules folder.

#### 1. Jupyter Notebook script to be used separately or within ArcGIS Pro.
##### NR6920-zientek-project-juputer-notebook.ipynb 
This notebook has detailed instructions on how the tool works and the type and format of the data required to successfully run. It was written for someone with limited python knowledge as only the folder path and out file name need to be entered in the variables listed in the first code block.

#### 2. Jupyter Notebook with individual functions created for the Add Field Fill In Date script and for the Erase and Merge script.
##### NR6920-zientek-project-function.ipynb 
Two functions were created, one that adds a field ('Date') to each shapefile, then fills in the rows under the new field with the date (format YYYYMMDD) that is at beginning of each file name.  The second function erases the previous date shapefile from the one immediately following and creates a new shapefile with "_eraseYYYYMMDD" appended to the end of the file with the YYYYMMDD signifying the date of the shapefile used as the erase feature.  The initial date (earliest date) shapefile and the erase shapefiles are merged into a single shapefile.

#### 3. Jupyter Notebook and Module with tools created for the Add Field Fill In Date script and for the Erase and Merge script.
##### NR6920-zientek-project-module.ipynb 
##### fire_tool.py
A module (fire_tools) was created that holds two tools: fp_add_date_field and fp_erase_merge.


